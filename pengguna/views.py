from django.http.response import FileResponse
from django.shortcuts import redirect, render
from django.contrib import messages
from django.db import connections
from psycopg2.extras import RealDictCursor
import io
from reportlab.pdfgen import canvas

# Create your views here.
def dashboard(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_warga' in request.session):
        return redirect("/")

    return render(request, 'index-pengguna.html')

def kartu_vaksin(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_warga' in request.session):
        return redirect("/")

    email = request.session.get('email')
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("""
    SELECT KV.no_sertifikat, KV.status_tahapan, W.nama_lengkap 
    FROM KARTU_VAKSIN KV
    JOIN TIKET T ON KV.email = T.email AND KV.no_tiket = T.no_tiket
    JOIN WARGA W ON T.email = W.email
    WHERE W.email = %s
    ORDER BY SUBSTRING(KV.status_tahapan, 7)::INT ASC;
    """, (email, ))
    list_kartu = cur.fetchall()

    cur.close()
    conn.close()

    list_kartu = [dict(row) for row in list_kartu]

    context = {"list_kartu": list_kartu}
    return render(request, 'kartu_vaksin.html', context)

def download_kartu_vaksin(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_warga' in request.session):
        return redirect("/")

    # Diadaptasi dari https://docs.djangoproject.com/en/3.2/howto/outputting-pdf/
    if request.method == 'POST':
        nama = request.POST.get('nama')
        no_sertifikat = request.POST.get('no_sertifikat')
        tahap = request.POST.get('tahap')

        buffer = io.BytesIO()

        p = canvas.Canvas(buffer)
        p.setFont("Helvetica-Bold", 18, None)
        p.drawString(100, 750, "Kartu Vaksin")

        p.line(85, 730, 515, 730)
        p.setFont("Helvetica", 12, None)
        p.drawString(100, 700, f"Nama: {nama}")
        p.drawString(100, 670, f"Nomor sertifikat: {no_sertifikat}")
        p.drawString(100, 640, f"Status tahapan vaksin: {tahap}")

        p.showPage()
        p.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True)


def jadwal_vaksin(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_warga' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)
    
    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("SELECT penjadwalan.kode_instansi, instansi.nama_instansi, penjadwalan.tanggal_waktu, penjadwalan.jumlah, penjadwalan.kategori_penerima, penjadwalan.kode_lokasi, lokasi_vaksin.nama as nama_lokasi FROM penjadwalan JOIN instansi ON penjadwalan.kode_instansi = instansi.kode JOIN lokasi_vaksin ON penjadwalan.kode_lokasi = lokasi_vaksin.kode WHERE penjadwalan.status = 'pengajuan disetujui';")
    jadwal_list = cur.fetchall()

    cur.close()
    conn.close()

    jadwal_list = [dict(row) for row in jadwal_list]

    context = {"jadwal_list": jadwal_list}
    return render(request, 'jadwal_vaksin.html', context)

def create_tiket_pengguna(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_warga' in request.session):
        return redirect("/")

    if request.method == 'POST':
        email = request.session.get('email')
        kode_instansi = request.POST.get('kode-instansi')
        tanggal_waktu = request.POST.get('tanggal-waktu')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("SELECT no_tiket FROM tiket ORDER BY LENGTH(no_tiket) DESC, no_tiket DESC LIMIT 1;")
        no_tiket = cur.fetchone()
        no_tiket = dict(no_tiket)['no_tiket']
        no_tiket = no_tiket[:3] + "{0:0>3}".format(int(no_tiket[3:])+1)

        try:
            cur.execute("INSERT INTO tiket VALUES (%s, %s, %s, null, TO_TIMESTAMP(%s, 'DD Month YYYY HH24:MI'));", (email, no_tiket, kode_instansi, tanggal_waktu))
            conn.commit()
        except Exception as e:
            messages.error(request, e)
            return redirect(jadwal_vaksin)

        cur.close()
        conn.close()
    return redirect(tiket_pengguna)

def tiket_pengguna(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_warga' in request.session):
        return redirect("/")

    email = request.session.get('email')
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)
    cur.execute("SET SEARCH_PATH TO sivax;")   
    cur.execute(f"""select no_tiket,nama_instansi,tanggal_waktu,status,jumlah,kategori_penerima,nama,nama_status from
                    (select * from (select * from (select * from tiket t join instansi i on t.kode_instansi = i.kode) A 
                    join status_tiket on status_tiket.kode = A.kode_status 
                    where email = '{email}') B join penjadwalan on penjadwalan.kode_instansi =  B.kode_instansi) 
                    C join lokasi_vaksin on c.kode_lokasi = lokasi_vaksin.kode ;""")
    
    list_tiket = cur.fetchall()

    list_tiket = [dict(row) for row in list_tiket]
    
    cur.close()
    conn.close()
    context = {"list_tiket": list_tiket}

    return render(request,'my_tiket.html',context)