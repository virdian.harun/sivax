from django.urls import path
from .views import *

urlpatterns = [
    path('', dashboard, name='dashboard-pengguna'),
    path('kartu/', kartu_vaksin, name='kartu-pengguna'),
    path('kartu/download', download_kartu_vaksin, name='download-kartu-pengguna'),
    path('jadwal/', jadwal_vaksin, name='jadwal-pengguna'),
    path('tiket-pengguna/',tiket_pengguna,name='tiket-pengguna'),
    path('tiket-pengguna/create', create_tiket_pengguna, name='create-tiket-pengguna')
]
