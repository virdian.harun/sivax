$(document).ready(function() {
    var cols;
    $('.toggleModal').click(function() {
        cols = $(this).parents("tr").children("td");
        $('#nama').text(cols[3].innerHTML);
        $('#noSerti').text(cols[1].innerHTML);
        $('#tahap').text(cols[2].innerHTML);
        $('#detail-kartu').modal('show');
    })

    $('#unduh').click(function() {
        $.ajax({
            url: '/pengguna/kartu/download',
            type: 'POST',
            headers: {
                'X-CSRFToken': getCookie('csrftoken')
            },
            xhrFields: {responseType: 'blob'},
            data: {
                'nama': cols[3].innerHTML,
                'no_sertifikat': cols[1].innerHTML,
                'tahap': cols[2].innerHTML
            },
            success: function (data) {
                var blob = new Blob([data]);
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = cols[1].innerHTML + "-" + cols[3].innerHTML + ".pdf";
                link.click();
                $('#detail-kartu').modal('hide');

            }, error: function (){
                alert("Download failed");
            }
        });
    })

    $('#cancel').click(function() {
        $('#detail-kartu').modal('hide');
    })
})

// Diadaptasi dari https://docs.djangoproject.com/en/3.2/ref/csrf/
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}