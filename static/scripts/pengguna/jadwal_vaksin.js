$(document).ready(function() {

    $('.daftar-button').click(function() {
        var cols = $(this).parents("tr").children("td");
        $('#nama-instansi').val($(cols[1]).text());
        $('#tanggal-waktu').val($(cols[2]).text());
        $('#kuota').val($(cols[3]).text());
        $('#kategori-penerima').val($(cols[4]).text());
        $('#lokasi-vaksin').val($(cols[5]).text());
        $('#kode-instansi').val($(cols[6]).text());

        $('#daftar-jadwal-modal').modal('show');
    })

    $('#cancel').click(function() {
        $('#daftar-jadwal-modal').modal('hide');
    })
})