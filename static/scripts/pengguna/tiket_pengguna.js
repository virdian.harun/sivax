$(document).ready(function() {
    $('.toggleModal').click(function() {
        var $row = $(this).closest("tr");  
        var $tds = $row.find("td");
        var tmp = new Array(9);
        var counter = 0;
        $.each($tds, function() {
            tmp[counter++] = $(this).text();
        });
        $('#nama-instansi').val(tmp[1]);
        $('#tanggal-waktu').val(tmp[2]);
        $('#kuota').val(tmp[7]);
        $('#kategori-penerima').val(tmp[4]);
        $('#lokasi-vaksin').val(tmp[5]);
        $('#nomor-tiket').val(tmp[0]);
        $('#status-tiket').val(tmp[6]);
        $('#detail-jadwal').modal('show');
    })

    $('#cancel').click(function() {
        $('#detail-jadwal').modal('hide');
    })
})