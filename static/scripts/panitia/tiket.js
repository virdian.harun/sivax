$(document).ready(function() {
    var cols;
    $('.toggleModal').on('click', function(){
        cols = $(this).parents("tr").children("td");
        $('#namaInstansi').val(cols[5].innerHTML);
        $('#tglWaktu').val(cols[3].innerHTML);
        $('#kuota').val(cols[6].innerHTML);
        $('#kategori').val(cols[7].innerHTML);
        $('#lokasi').val(cols[8].innerHTML);
        $('#noTiket').val(cols[1].innerHTML);

        if ($(this).hasClass('btn-detail')) {
            $('#statustiket').val(cols[4].innerHTML);
            $('.case-ubah').hide();
            $('.case-detail').show();
            $('.modal-title').html("Detail Tiket");

        } else {
            $('.modal-title').html("Ubah Status Tiket");
            if (cols[4].innerHTML == "Terdaftar") {
                $('#siap-vaksin').prop('checked', true);
                $('#tidak-lolos').prop('checked', false);
                $('.case-detail').hide();
                $('.case-siap').hide();
                $('.case-terdaftar').show();
            } else {
                $('#selesai-vaksin').prop('checked', true);
                $('.case-detail').hide();
                $('.case-terdaftar').hide();
                $('.case-siap').show();
                
            }
        }
        $('#modal-tiket').modal('show');

        $('#simpan').click(function() {
            var status;
            if ($('#siap-vaksin').is(":checked")) {
                status = "Siap Vaksin";
            } else if ($('#tidak-lolos').is(":checked")) {
                status = "Tidak Lolos Screening";
            } else {
                status = "Selesai Vaksin";
            }

            $.ajax({
                url: '/panitia/tiket/update',
                type: 'POST',
                headers: {
                    'X-CSRFToken': getCookie('csrftoken')
                },
                data: {
                    'email' : cols[9].innerHTML,
                    'noTiket': cols[1].innerHTML,
                    'status': status.toLowerCase(),
                },
                dataType: 'json',
                success: function (data) {
                    cols[4].innerHTML = status;
                    if (status != "Siap Vaksin") {
                        var html = 
                        "<button type='button' class='btn btn-primary toggleModal btn-detail'>Detail</button>";
                        cols[10].innerHTML = html;
                    }

                }, error: function (){
                    alert("Update failed");
                }
            });
            
            $('#modal-tiket').modal('hide');
        })
    
        $('#cancel').click(function() {
            $('#modal-tiket').modal('hide');
        })
    })  
})

// Diadaptasi dari https://docs.djangoproject.com/en/3.2/ref/csrf/
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}