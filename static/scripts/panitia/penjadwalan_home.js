$(document).ready(function(){
    $("#search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#info-penjadwalan tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });

    $('.toggleModal').click(function() {
      if ($(this).hasClass('btn-detail')) {
          $('.case-ubah').hide();
          $('.case-detail').show();
          $('.case-normal').show();
          $('.case-tambah').hide();



          $('#namaInstansi').attr("disabled", true);
          $('#tglWaktu').attr("disabled", true);
          $('#kuota').attr("disabled", true);
          $('#kategori').attr("disabled", true);
          $('#lokasi').attr("disabled", true);

          $('.modal-title').html("Detail Penjadwalan");
      } 
      else if ($(this).hasClass('btn-tambah')) {
        $('.case-ubah').hide();
        $('.case-detail').hide();
        $('.case-normal').hide();
        $('.case-tambah').show();

      }
      else {
          $('.case-normal').show();
          $('.case-ubah').show();
          $('.case-tambah').hide();

          $('#namaInstansi').attr("disabled", false);
          $('#tglWaktu').attr("disabled", false);
          $('#kuota').attr("disabled", false);
          $('#kategori').attr("disabled", false);
          $('#lokasi').attr("disabled", false);
          $('.case-detail').hide();
          $('.case-terdaftar').hide();
          $('.case-siap').show();
          $('.modal-title').html("Ubah Status Penjadwalan");
      }

      $('#modal-penjadwalan').modal('show');
  })

  $('#simpan').click(function() {
      $('#modal-penjadwalan').modal('hide');
  })

  $('#cancel').click(function() {
      $('#modal-penjadwalan').modal('hide');
  })
  });