$(document).ready(function() {
    $('.add-button').click(function() {
        $('#create-nama-instansi').val("");
        $('#create-nomor-telepon-instansi').val("");
        $('.faskes-field').show();
        $('.non-faskes-field').hide();
        $('#create-instansi-modal').modal('show');
    })

    $('.detail-button').click(function() {
        var cols = $(this).parents("tr").children("td");
        $('#detail-kode-instansi').val($(cols[1]).text());
        $('#detail-nama-instansi').val($(cols[2]).text());
        $('#detail-nomor-telepon-instansi').val($(cols[3]).text());
        $('#detail-jenis-instansi').val($(cols[4]).text());
        $('#detail-tipe-instansi-faskes').val($(cols[5]).text());
        $('#detail-status-kepemilikan-instansi-faskes').val($(cols[6]).text());
        $('#detail-kategori-instansi-non-faskes').val($(cols[7]).text());
        if ($(cols[4]).text() === "faskes") {
            $('.faskes-field').show();
            $('.non-faskes-field').hide();
        } else if ($(cols[4]).text() === "non-faskes") {
            $('.faskes-field').hide();
            $('.non-faskes-field').show();
        }
        $('#detail-instansi-modal').modal('show');
    })

    $('.update-button').click(function() {
        var cols = $(this).parents("tr").children("td");
        $('#update-kode-instansi').val($(cols[1]).text());
        $('#update-nama-instansi').val($(cols[2]).text());
        $('#update-nomor-telepon-instansi').val($(cols[3]).text());
        $('#update-jenis-instansi').val($(cols[4]).text());
        $('#update-tipe-instansi-faskes').val($(cols[5]).text());
        $('#update-status-kepemilikan-instansi-faskes').val($(cols[6]).text());
        $('#update-kategori-instansi-non-faskes').val($(cols[7]).text());
        if ($(cols[4]).text() === "faskes") {
            $('.faskes-field').show();
            $('.non-faskes-field').hide();
        } else if ($(cols[4]).text() === "non-faskes") {
            $('.faskes-field').hide();
            $('.non-faskes-field').show();
        }
        $('#update-instansi-modal').modal('show');
    })

    $('.delete-button').click(function() {
        var cols = $(this).parents("tr").children("td");
        $('#delete-kode-instansi').val($(cols[1]).text());
        $('.delete-text').html("Anda yakin untuk menghapus instansi " + $(cols[1]).text() + "?");
        $('#delete-instansi-modal').modal('show');
    })

    $('.jenis-instansi').change(function() {
        var jenis = $(this).val();
        if (jenis === "faskes") {
            $('.faskes-field').show();
            $('.non-faskes-field').hide();
        } else if (jenis === "non-faskes") {
            $('.faskes-field').hide();
            $('.non-faskes-field').show();
        }
    })

    $('.cancel-button').click(function() {
        $('#create-instansi-modal').modal('hide');
        $('#detail-instansi-modal').modal('hide');
        $('#update-instansi-modal').modal('hide');
        $('#delete-instansi-modal').modal('hide');
    })
})