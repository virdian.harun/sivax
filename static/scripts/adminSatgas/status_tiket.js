$(document).ready(function() {
    $('.add-button').click(function() {
        $('#create-nama-status').val("");
        $('#create-status-tiket-modal').modal('show');
    })

    $('.update-button').click(function() {
        var cols = $(this).parents("tr").children("td");
        $('#update-kode-status').val($(cols[1]).text());
        $('#update-nama-status').val($(cols[2]).text());
        $('#update-status-tiket-modal').modal('show');
    })

    $('.delete-button').click(function() {
        var cols = $(this).parents("tr").children("td");
        $('#delete-kode-status').val($(cols[1]).text());
        $('#delete-nama-status').val($(cols[2]).text());
        $('.delete-text').html("Anda yakin untuk menghapus status tiket " + $(cols[1]).text() + "?");
        $('#delete-status-tiket-modal').modal('show');
    })

    $('.cancel-button').click(function() {
        $('#create-status-tiket-modal').modal('hide');
        $('#update-status-tiket-modal').modal('hide');
        $('#delete-status-tiket-modal').modal('hide');
    })
})