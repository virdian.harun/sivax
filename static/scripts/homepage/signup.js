$(document).ready(() => {
  /*handle changes on the selected input */
  var choices = $("#jenis-akun").val();
  var href = $("#lanjut").attr("href");
  $("#jenis-akun").on("change", function () {
    choices = this.value;
    href = `\\signup/${choices}`;
    $("#lanjut").attr("href", href)
  });
});
