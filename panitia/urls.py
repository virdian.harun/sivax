from django.urls import path
from .views import  penjadwalan_home, tiket, update_tiket

urlpatterns = [
    path('tiket/', tiket, name='tiket-nakes'),
    path('tiket/update', update_tiket, name='update-tiket-nakes'),
    path('',penjadwalan_home,name='dashboard-panitia'),
]