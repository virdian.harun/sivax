from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from psycopg2.extras import RealDictCursor
from django.db import connections

# Create your views here.
def tiket(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_nakes' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("""
    SELECT T.no_tiket, W.nama_lengkap, T.tgl_waktu, S.nama_status status, I.nama_instansi, 
    P.jumlah kuota, P.kategori_penerima kategori, L.nama lokasi, W.email FROM TIKET T
    JOIN WARGA W ON T.email = W.email
    JOIN STATUS_TIKET S ON T.kode_status = S.kode
    JOIN PENJADWALAN P ON T.kode_instansi = P.kode_instansi 
    AND T.tgl_Waktu = P.tanggal_Waktu
    JOIN INSTANSI I ON P.kode_instansi = I.kode
    JOIN LOKASI_VAKSIN L ON P.kode_lokasi = L.kode
    ORDER BY SUBSTRING(T.no_tiket, 4)::INT ASC;
    """)
    list_tiket = cur.fetchall()

    cur.close()
    conn.close()

    list_tiket = [dict(row) for row in list_tiket]

    context = {"list_tiket": list_tiket}
    return render(request, 'tiket.html', context)

def update_tiket(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_nakes' in request.session):
        return redirect("/")

    if request.method == 'POST':
        email = request.POST.get('email')
        no_tiket = request.POST.get('noTiket')
        nama_status = request.POST.get('status')
        print(email)
        print(no_tiket)
        print(nama_status);

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("""
        SELECT kode FROM STATUS_TIKET
        WHERE nama_status = %s
        ORDER BY kode ASC;
        """, (nama_status, ))
        kode_status = cur.fetchone()
        kode_status = dict(kode_status)
        kode_status = kode_status["kode"]

        cur.execute("""
        UPDATE TIKET
        SET kode_status = %s 
        WHERE email = %s
        AND no_tiket = %s;
        """, (kode_status, email, no_tiket))
        conn.commit()

        cur.close()
        conn.close()
        return JsonResponse({'status': 'success'})

def penjadwalan_home(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_panitia' in request.session):
        return redirect("/")
    return render(request,'penjadwalan_home.html')

