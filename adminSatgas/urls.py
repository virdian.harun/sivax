from django.urls import path
from .views import *

urlpatterns = [
    path('', dashboard, name='dashboard-satgas'),
    path('status-tiket/', status_tiket, name='status-tiket-satgas'),
    path('status-tiket/create', create_status_tiket, name='create-status-tiket-satgas'),
    path('status-tiket/update', update_status_tiket, name='update-status-tiket-satgas'),
    path('status-tiket/delete', delete_status_tiket, name='delete-status-tiket-satgas'),
    path('instansi/', instansi, name='instansi-satgas'),
    path('instansi/create', create_instansi, name='create-instansi-satgas'),
    path('instansi/update', update_instansi, name='update-instansi-satgas'),
    path('instansi/delete', delete_instansi, name='delete-instansi-satgas'),
    path('penjadwalan/',penjadwalan,name='penjadwalan-satgas'),
    path('daftarvaksin/', daftarvaksin, name='daftar-vaksin'),
    path('daftarvaksin/delete/<str:id>', deletevaksin, name='delete-vaksin'),
    path('daftarvaksin/tambah', tambahvaksin, name='tambah-vaksin'),
    path('daftarvaksin/tambahvaksin', tambahjenisvaksin, name='tambah-jenis-vaksin'),
    path('penjadwalan/atur-distribusi/<int:pk>',distribusi),
    path('daftarvaksin/daftarupdatestok/<str:id>', daftarupdatestok, name='daftar-update-stok')
]