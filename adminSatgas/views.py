from django.shortcuts import render, redirect

import psycopg2
from psycopg2.extras import RealDictCursor
from django.db import connections
from datetime import date
# Create your views here.

def dashboard(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")
    return render(request, 'index-satgas.html')

def status_tiket(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")   
    cur.execute("SELECT * FROM status_tiket;")
    status_list = cur.fetchall()

    cur.execute("SELECT MAX(kode) FROM status_tiket;")
    kode = cur.fetchone()

    cur.close()
    conn.close()

    status_list = [dict(row) for row in status_list]

    kode = dict(kode)['max']
    if kode is None:
        kode =1
    else:
        kode = int(kode) + 1

    context = {"status_list": status_list, "kode": kode}
    return render(request, 'status_tiket.html', context)

def create_status_tiket(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    if request.method == 'POST':
        kode = request.POST.get('create-kode-status')
        nama = request.POST.get('create-nama-status')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("INSERT INTO status_tiket VALUES (%s, %s);", (kode, nama))
        conn.commit()

        cur.close()
        conn.close()

    return redirect(status_tiket)


def update_status_tiket(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")
        
    if request.method == 'POST':
        kode = request.POST.get('update-kode-status')
        nama = request.POST.get('update-nama-status')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("UPDATE status_tiket SET nama_status = %s WHERE kode = %s;", (nama, kode))
        conn.commit()

        cur.close()
        conn.close()

    return redirect(status_tiket)


def delete_status_tiket(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    if request.method == 'POST':
        kode = request.POST.get('delete-kode-status')
        nama = request.POST.get('delete-nama-status')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("DELETE FROM status_tiket WHERE kode = %s;", (kode,))
        conn.commit()

        cur.close()
        conn.close()

    return redirect(status_tiket)

def instansi(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("select * from instansi join instansi_telepon on instansi.kode = instansi_telepon.kode_instansi left join instansi_faskes on instansi.kode = instansi_faskes.kode_instansi left  join instansi_non_faskes on instansi.kode = instansi_non_faskes.kode_instansi;")
    status_list = cur.fetchall()

    cur.execute("SELECT MAX(kode) FROM instansi;")
    kode = cur.fetchone()

    cur.close()
    conn.close()

    status_list = [dict(row) for row in status_list]
    kode = dict(kode)['max']
    if kode is None:
        kode = 'D001'
    else:
        kode = kode[0] + "{0:0>3}".format(int(kode[1:])+1)

    context = {"instansi_list": status_list, "kode": kode}
    return render(request, 'instansi.html', context)

def create_instansi(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    if request.method == 'POST':
        kode = request.POST.get('create-kode-instansi')
        nama = request.POST.get('create-nama-instansi')
        no_telp = request.POST.get('create-nomor-telepon-instansi')
        jenis = request.POST.get('create-jenis-instansi')
        tipe = request.POST.get('create-tipe-instansi-faskes')
        status_kepemilikan = request.POST.get('create-status-kepemilikan-instansi-faskes')
        kategori = request.POST.get('create-kategori-instansi-non-faskes')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("INSERT INTO instansi VALUES (%s, %s);", (kode, nama))
        cur.execute("INSERT INTO instansi_telepon VALUES (%s, %s);", (kode, no_telp))

        if jenis == "faskes":
            cur.execute("INSERT INTO instansi_faskes VALUES (%s, %s, %s);", (kode, tipe, status_kepemilikan))
        if jenis == "non-faskes":
            cur.execute("INSERT INTO instansi_non_faskes VALUES (%s, %s);", (kode, kategori))

        conn.commit()

        cur.close()
        conn.close()

    return redirect(instansi)


def update_instansi(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    if request.method == 'POST':
        kode = request.POST.get('update-kode-instansi')
        nama = request.POST.get('update-nama-instansi')
        no_telp = request.POST.get('update-nomor-telepon-instansi')
        jenis = request.POST.get('update-jenis-instansi')
        tipe = request.POST.get('update-tipe-instansi-faskes')
        status_kepemilikan = request.POST.get('update-status-kepemilikan-instansi-faskes')
        kategori = request.POST.get('update-kategori-instansi-non-faskes')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("UPDATE instansi SET nama_instansi = %s WHERE kode = %s;", (nama, kode))
        cur.execute("UPDATE instansi_telepon SET no_telp = %s WHERE kode_instansi = %s;", (no_telp, kode))

        if jenis == "faskes":
            cur.execute(
                "INSERT INTO instansi_faskes VALUES (%s, %s, %s) ON CONFLICT (kode_instansi) DO UPDATE SET tipe = %s, statuskepemilikan = %s WHERE instansi_faskes.kode_instansi = %s;", (kode, tipe, status_kepemilikan, tipe, status_kepemilikan, kode))
            cur.execute(
                "DELETE FROM instansi_non_faskes WHERE kode_instansi = %s;", (kode,))
        if jenis == "non-faskes":
            cur.execute(
                "INSERT INTO instansi_non_faskes VALUES (%s, %s) ON CONFLICT (kode_instansi) DO UPDATE SET kategori = %s WHERE instansi_non_faskes.kode_instansi = %s;", (kode, kategori, kategori, kode))
            cur.execute(
                "DELETE FROM instansi_faskes WHERE kode_instansi = %s;", (kode,))

        conn.commit()

        cur.close()
        conn.close()

    return redirect(instansi)


def delete_instansi(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    if request.method == 'POST':
        kode = request.POST.get('delete-kode-instansi')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("DELETE FROM instansi WHERE kode = %s;", (kode,))

        conn.commit()

        cur.close()
        conn.close()

    return redirect(instansi)

def penjadwalan(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    return render(request,'penjadwalan.html')

def daftarvaksin(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("SELECT * FROM vaksin;")
    vaksin_list = cur.fetchall()

    cur.execute("SELECT MAX(kode) FROM vaksin;")
    kodevaksin = cur.fetchone()

    vaksin_list = [dict(row) for row in vaksin_list]
    kodevaksin = dict(kodevaksin)['max']
    if kodevaksin is None:
        kodevaksin = 'vcn01'
    else:
        kodevaksin = kodevaksin[0:3] + "{0:0>2}".format(int(kodevaksin[3:])+1)
    context = {"vaksin_list":vaksin_list, "kodevaksin" : kodevaksin}

    return render(request,'vaksin.html', context)

def deletevaksin(request,id):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)
    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("DELETE FROM vaksin WHERE kode = %s AND %s NOT IN (select distinct kode_vaksin from distribusi);", (id, id))

    return redirect(daftarvaksin)
def tambahjenisvaksin(request):
    if request.method == 'POST':
        kode = request.POST.get('create-kode-vaksin')
        nama = request.POST.get('create-nama-vaksin')
        produsen = request.POST.get('create-nama-produsen')
        noedar = request.POST.get('create-noedar')
        stok = request.POST.get('jumlah-stok')
        frekuensi = request.POST.get('create-frekuensi')

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("INSERT INTO vaksin VALUES(%s, %s, %s, %s, %s, %s);", (kode, nama, produsen, noedar, stok, frekuensi))

        conn.commit()

        cur.close()
        conn.close()

    return redirect(daftarvaksin)
def tambahvaksin(request):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    if request.method== 'POST':
        email = request.session.get('email')
        kodevaksin = request.POST.get('kode-vaksin')
        jumlah = request.POST.get('jumlah-update')
        
        today = date.today()
        d4 = today.strftime("%Y-%b-%d")

        conn = connections['default']
        conn.ensure_connection()
        cur = conn.connection.cursor(cursor_factory=RealDictCursor)

        cur.execute("SET SEARCH_PATH TO sivax;")
        cur.execute("INSERT INTO update_stok VALUES (%s, %s, %s, %s);", (email, d4, jumlah, kodevaksin, ))
        cur.execute("UPDATE VAKSIN SET stok = stok + %s WHERE kode = %s;", (jumlah, kodevaksin, ))
    return redirect(daftarvaksin)

def distribusi(request,pk):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")
    return render(request,'distribusi.html')

def daftarupdatestok(request, id):
    if not ('email' in request.session):
        return redirect("/login")
    elif not ('is_admin' in request.session):
        return redirect("/")

    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("SELECT * FROM update_stok WHERE kode_vaksin = %s;", (id, ))
    updatevaksin_list = cur.fetchall()
    updatevaksin_list = [dict(row) for row in updatevaksin_list]

    cur2 = conn.connection.cursor(cursor_factory=RealDictCursor)
    cur2.execute("SELECT nama FROM vaksin WHERE kode = %s;", (id, ))
    namavaksin = cur2.fetchone()
    context = {"updatevaksin_list":updatevaksin_list, "nama":namavaksin}

    return render(request, 'update_stok_vaksin.html', context)