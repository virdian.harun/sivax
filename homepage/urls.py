from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='homepage'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('signup/',signup,name='signup'),
    path('signup/admin',signup_admin,name='signup-admin'),
    path('signup/warga',signup_warga,name='signup-warga'),
    path('signup/panitia',signup_panitia,name='signup-panitia'),
]
