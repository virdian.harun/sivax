from django.shortcuts import render, redirect
import psycopg2
from psycopg2.extras import RealDictCursor
from django.db import connections
# Create your views here.


# gak tau kenapa, gak bisa dipake 
# def is_logged_in(request):
#   if 'email' in request.session:
#         if 'is_admin' in request.session:
#             return redirect('/satgas')
#         if 'is_panitia' in request.session:
#             return redirect('/panitia')
#         if 'is_warga' in request.session:
#             return redirect('/pengguna')

def index(request):
    if 'email' in request.session:
        if 'is_admin' in request.session:
            return redirect('/satgas')
        if 'is_panitia' in request.session:
            return redirect('/panitia')
        if 'is_warga' in request.session:
            return redirect('/pengguna')

    return render(request, 'index.html')

def login(request):
    if 'email' in request.session:
        if 'is_admin' in request.session:
            return redirect('/satgas')
        if 'is_panitia' in request.session:
            return redirect('/panitia')
        if 'is_warga' in request.session:
            return redirect('/pengguna')

    response = {}
    response['error'] = False

    if request.method == 'POST':
        email = request.POST['form-email']
        password = request.POST['form-password']

        if verified(email, password):
            request.session['email'] = email
            if is_warga(email):
                request.session['is_warga'] = True
            if is_admin(email):
                request.session['is_admin'] = True
                return redirect('/satgas')
            if is_panitia(email):
                if is_nakes(email):
                    request.session['is_nakes'] = True
                request.session['is_panitia'] = True
                return redirect('/panitia')
            return redirect('/pengguna')
        else:
            response['error'] = True

    return render(request, 'login.html', response)

def logout(request):
    if "email" in request.session:
        request.session.flush()
        return redirect('/')
    return redirect('/')

def verified(email, password):
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute("SELECT * FROM pengguna WHERE email = %s AND password = %s;", (email, password))
    pengguna = cur.fetchone()
    
    if pengguna is None:
        cur.close()
        conn.close()
        return False
    
    pengguna = dict(pengguna)
    if pengguna['status_verifikasi'] == 'belum terverifikasi':
        cur.execute("UPDATE pengguna SET status_verifikasi = 'sudah terverifikasi' WHERE email = %s;", (email,))
        conn.commit()

    cur.close()
    conn.close()
    return True

def is_warga(email):
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute(
        "SELECT * FROM warga WHERE email = %s;", (email,))
    pengguna = cur.fetchone()

    cur.close()
    conn.close()

    if pengguna:
        return True
    else:
        return False

def is_admin(email):
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute(
        "SELECT * FROM admin_satgas WHERE email = %s;", (email,))
    pengguna = cur.fetchone()

    cur.close()
    conn.close()

    if pengguna:
        return True
    else:
        return False

def is_panitia(email):
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute(
        "SELECT * FROM panitia_penyelenggara WHERE email = %s;", (email,))
    pengguna = cur.fetchone()

    cur.close()
    conn.close()

    if pengguna:
        return True
    else:
        return False

def is_nakes(email):
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)

    cur.execute("SET SEARCH_PATH TO sivax;")
    cur.execute(
        "SELECT * FROM nakes WHERE email = %s;", (email,))
    pengguna = cur.fetchone()

    cur.close()
    conn.close()

    if pengguna:
        return True
    else:
        return False

def is_sign_up(email,cur):
    cur.execute('set search_path to sivax;')
    cur.execute(f"select email from pengguna where email = '{email}'")
    email = cur.fetchone()

    if email:
        return True

    return False
    

# sebenarnya redundant tiap kali admin, panitia di sign up warga, bisa panggil metode sign up warga baru sign up admin/panitia
# biar hemat. 
def signup(request):
    if 'email' in request.session:
        if 'is_admin' in request.session:
            return redirect('/satgas')
        if 'is_panitia' in request.session:
            return redirect('/panitia')
        if 'is_warga' in request.session:
            return redirect('/pengguna')

    return render(request,'signup.html')

def signup_admin(request):
    if 'email' in request.session:
        if 'is_admin' in request.session:
            return redirect('/satgas')
        if 'is_panitia' in request.session:
            return redirect('/panitia')
        if 'is_warga' in request.session:
            return redirect('/pengguna')
    
    context = {}
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)
    cur.execute('set search_path to sivax;')
    cur.execute('select kode,nama_instansi from instansi;')

    kode_instansi = cur.fetchall()
    kode_instansi = [dict(row) for row in kode_instansi]
    context['kode_instansi'] = kode_instansi

    if request.method == 'POST':
        email = request.POST.get('email-input') # ke pengguna, admin
        password =  request.POST.get('password-input')#ke pengguna only
        no_telp =  request.POST.get('no-telp') # ke pengguna
        no_petugas = request.POST.get('no-petugas') #ke satgas meibi 
        nik = request.POST.get('nik-input') # ke warga
        nama = request.POST.get('nama-input')# ke warga 
        jenis_kelamin = request.POST.get('jenis-kelamin') # ke warga
        no_bangunan = request.POST.get('no-bangunan-input') # ke warga
        nama_jalan = request.POST.get('nama-jalan-input') # ke warga 
        kelurahan = request.POST.get('nama-kelurahan-input')# ke warga
        kecamatan = request.POST.get('nama-kecamatan-input') # ke warga
        kabupaten = request.POST.get('nama-kabupaten-input') # ke warga
        instansi = request.POST.get('jenis-instansi-input') # ke warga
        print(instansi)

        if is_sign_up(email,cur):
            context['error'] = True
            context['error_message'] = 'the email has already taken'
            return render(request,'signup_admin.html',context)
            # mendingan ga usah, try except aja di curnya

        try:
            # insert value into pengguna
            cur.execute(f"insert into pengguna values ('{email}',{no_telp},'{password}','belum terverifikasi')")
            # insert value into admin
            cur.execute(f"insert into admin_satgas values ('{email}','{no_petugas}')")
            # insert value into warga
            # warga (Email NIK NamaLengkap JenisKelamin No,Jalan Kelurahan Kecamatan KabKot Instansi)
            cur.execute(f"""insert into warga values 
            ('{email}',{nik},'{nama}','{jenis_kelamin}','{no_bangunan}','{nama_jalan}',
            '{kelurahan}','{kecamatan}','{kabupaten}','{instansi}')""")
        except Exception as e:
            context['error'] = True
            context['error_message'] = e 
            return render(request,'signup_admin.html',context)


        cur.close()
        conn.close()
        return redirect('/')

    return render(request,'signup_admin.html',context)

def signup_panitia(request):
    if 'email' in request.session:
        if 'is_admin' in request.session:
            return redirect('/satgas')
        if 'is_panitia' in request.session:
            return redirect('/panitia')
        if 'is_warga' in request.session:
            return redirect('/pengguna')

    context = {}
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)
    cur.execute('set search_path to sivax;')
    cur.execute('select kode,nama_instansi from instansi;')

    kode_instansi = cur.fetchall()
    kode_instansi = [dict(row) for row in kode_instansi]
    context['kode_instansi'] = kode_instansi

    if request.method == 'POST':
        email = request.POST.get('email-input') # ke pengguna, admin
        password =  request.POST.get('password-input')#ke pengguna only
        no_telp =  request.POST.get('no-telp') # ke pengguna
        nik = request.POST.get('nik-input') # ke warga
        nama = request.POST.get('nama-input')# ke warga 
        jenis_kelamin = request.POST.get('jenis-kelamin') # ke warga
        no_bangunan = request.POST.get('no-bangunan-input') # ke warga
        nama_jalan = request.POST.get('nama-jalan-input') # ke warga 
        kelurahan = request.POST.get('nama-kelurahan-input')# ke warga
        kecamatan = request.POST.get('nama-kecamatan-input') # ke warga
        kabupaten = request.POST.get('nama-kabupaten-input') # ke warga
        instansi = request.POST.get('jenis-instansi-input') # ke warga
        panitia_kesehatan = request.POST.get('flexRadioDefault') #untuk cek dia kesehatan atau bukan
        no_str = request.POST.get('no-str')
        tipe = request.POST.get('tipe-petugas-input')


        if is_sign_up(email,cur):
            context['error'] = True
            context['error_message'] = 'the email has already taken'
            return render(request,'signup_panitia.html',context)
            # mendingan ga usah, try except aja di curnya

        try:
            # insert value into pengguna
            cur.execute(f"insert into pengguna values ('{email}',{no_telp},'{password}','belum terverifikasi')")
            #insert ke panitia
            cur.execute(f"insert into panitia_penyelenggara values ('{email}','{nama}')")
            
            if panitia_kesehatan == 'yes':
                cur.execute(f"insert into nakes values ('{email}','{no_str}','{tipe}')")
            # insert value into warga
            # warga (Email NIK NamaLengkap JenisKelamin No,Jalan Kelurahan Kecamatan KabKot Instansi)
            cur.execute(f"""insert into warga values 
            ('{email}',{nik},'{nama}','{jenis_kelamin}','{no_bangunan}','{nama_jalan}',
            '{kelurahan}','{kecamatan}','{kabupaten}','{instansi}')
            """)
        except Exception as e:
            context['error'] = True
            context['error_message'] = e 
            return render(request,'signup_panitia.html',context)

        cur.close()
        conn.close()
        return redirect('/')
    return render(request,'signup_panitia.html',context)

def signup_warga(request):
    if 'email' in request.session:
        if 'is_admin' in request.session:
            return redirect('/satgas')
        if 'is_panitia' in request.session:
            return redirect('/panitia')
        if 'is_warga' in request.session:
            return redirect('/pengguna')

    context = {}
    conn = connections['default']
    conn.ensure_connection()
    cur = conn.connection.cursor(cursor_factory=RealDictCursor)
    cur.execute('set search_path to sivax;')
    cur.execute('select kode,nama_instansi from instansi;')

    kode_instansi = cur.fetchall()
    kode_instansi = [dict(row) for row in kode_instansi]
    context['kode_instansi'] = kode_instansi

    if request.method == 'POST':
        email = request.POST.get('email-input') # ke pengguna, admin
        password =  request.POST.get('password-input')#ke pengguna only
        no_telp =  request.POST.get('no-telp') # ke pengguna
        nik = request.POST.get('nik-input') # ke warga
        nama = request.POST.get('nama-input')# ke warga 
        jenis_kelamin = request.POST.get('jenis-kelamin') # ke warga
        no_bangunan = request.POST.get('no-bangunan-input') # ke warga
        nama_jalan = request.POST.get('nama-jalan-input') # ke warga 
        kelurahan = request.POST.get('nama-kelurahan-input')# ke warga
        kecamatan = request.POST.get('nama-kecamatan-input') # ke warga
        kabupaten = request.POST.get('nama-kabupaten-input') # ke warga
        instansi = request.POST.get('jenis-instansi-input') # ke warga

        if is_sign_up(email,cur):
            context['error'] = True
            context['error_message'] = 'the email has already taken'
            return render(request,'signup_warga.html',context)

        try:
            cur.execute(f"insert into pengguna values ('{email}',{no_telp},'{password}','belum terverifikasi')")
            cur.execute(f"""insert into warga values 
            ('{email}',{nik},'{nama}','{jenis_kelamin}','{no_bangunan}','{nama_jalan}',
            '{kelurahan}','{kecamatan}','{kabupaten}','{instansi}')
            """)
        except Exception as e:
            context['error'] = True
            context['error_message'] = e 
            return render(request,'signup_warga.html',context)

        cur.close()
        conn.close()
        return redirect('/')

    return render(request,'signup_warga.html',context)